apiVersion: v1
kind: Template
labels:
  app: maildropsmtp
  template: maildropsmtp-jenkins-pipeline
metadata:
  annotations:
    description: MaildropSMTP - Jenkinsfile
    iconClass: icon-nodejs
    openshift.io/display-name: MaildropSMTP CI
    tags: maildropsmtp
  name: maildropsmtp-jenkins-pipeline
objects:
- apiVersion: v1
  kind: BuildConfig
  metadata:
    annotations:
      description: Tests MaildropSMTP images
    name: maildropsmtp-jenkins-pipeline
  spec:
    strategy:
      jenkinsPipelineStrategy:
        jenkinsfile: |-
          def cloneProto   = "http"
          def podLabel     = "s2i-${UUID.randomUUID().toString()}"
          def privateRepo  = false
          def gitCommitMsg = ''
          def templateMark = 'maildropsmtp-jenkins-ci'
          def templateSel  = 'jenkins-ci-mark'
          pipeline {
              agent {
                  kubernetes {
                      cloud 'openshift'
                      label podLabel
                      containerTemplate {
                          name 'jnlp'
                          image "${OPENSHIFT_REGISTRY}/${CICD_NAMESPACE}/${JENKINS_SONAR_IMAGE}"
                      }
                      inheritFrom 'node'
                      serviceAccount 'jenkins'
                  }
              }
              options { timeout(time: 335, unit: 'MINUTES') }
              parameters {
                  string(defaultValue: '3', description: 'Max Retry', name: 'jobMaxRetry')
                  string(defaultValue: '1', description: 'Retry Count', name: 'jobRetryCount')
                  string(defaultValue: 'master', description: 'NodeJS Docker Image Tag', name: 'nodeTag')
                  string(defaultValue: '${OPENSHIFT_ROUTED_DOMAIN}', description: 'CI Router Root Domain', name: 'rootDomain')
                  string(defaultValue: 'master', description: 'MaildropSMTP Docker Image - Source Git Branch', name: 'buildBranch')
                  string(defaultValue: 'master', description: 'MaildropSMTP Docker Image - Source Git Hash', name: 'buildHash')
                  string(defaultValue: '${GIT_SOURCE_HOST}/${GIT_REPOSITORY}', description: 'MaildropSMTP Docker Image - Source Git Repository', name: 'buildRepo')
                  string(defaultValue: '${SONAR_ENDPOINT}', description: 'SonarQube Host Endpoint', name: 'sonarHost')
                  string(defaultValue: '${SONAR_PORT}', description: 'SonarQube Port', name: 'sonarPort')
                  string(defaultValue: '${SONAR_PROTO}', description: 'SonarQube Protocol', name: 'sonarProto')
              }
              stages {
                  stage('pre-cleanup') {
                      steps {
                          script {
                              openshift.withCluster() {
                                  openshift.withProject() {
                                      echo "cleaning up previous assets for maildropsmtp-${params.buildHash}"
                                      echo "Using project: ${openshift.project()}"
                                      openshift.selector("buildconfigs", [ "${templateSel}": "${templateMark}-${params.buildHash}" ]).delete()
                                      openshift.selector("configmaps", [ "${templateSel}": "${templateMark}-${params.buildHash}" ]).delete()
                                      openshift.selector("deploymentconfigs", [ "${templateSel}": "${templateMark}-${params.buildHash}" ]).delete()
                                      openshift.selector("routes", [ "${templateSel}": "${templateMark}-${params.buildHash}" ]).delete()
                                      openshift.selector("secrets", [ "${templateSel}": "${templateMark}-${params.buildHash}" ]).delete()
                                      openshift.selector("services", [ "${templateSel}": "${templateMark}-${params.buildHash}" ]).delete()
                                  }
                              }
                          }
                      }
                  }
                  stage('scan') {
                      steps {
                          script {
                              openshift.withCluster() {
                                  openshift.withProject() {
                                      def namespace = "${openshift.project()}"
                                      try {
                                          timeout(60) {
                                              def created
                                              def objectsFromTemplate
                                              def repoHost = params.buildRepo.split('/')[0]
                                              def templatePath = "/home/jenkins/workspace/${namespace}/${namespace}-maildropsmtp-jenkins-pipeline/tmpmaildropsmtp${params.buildBranch}/openshift"
                                              sh "git config --global http.sslVerify false"
                                              sh "rm -fr tmpmaildropsmtp${params.buildBranch}; mkdir -p tmpmaildropsmtp${params.buildBranch}"
                                              dir ("tmpmaildropsmtp${params.buildBranch}") {
                                                  try {
                                                      withCredentials([string(credentialsId: "git-${repoHost}", variable: 'GIT_TOKEN')]) {
                                                          cloneProto = "https"
                                                          privateRepo = true
                                                          echo "cloning ${params.buildRepo} over https, using ${repoHost} token"
                                                          try { git([ branch: "${params.buildBranch}", url: "https://${GIT_TOKEN}@${params.buildRepo}" ]) }
                                                          catch(e2) {
                                                              echo "Failed cloning ${params.buildRepo}#${params.buildBranch} - ${e2}"
                                                              throw e2
                                                          }
                                                      }
                                                  } catch(e) {
                                                      if (privateRepo != true) {
                                                          echo "caught ${e} - assuming no credentials required"
                                                          echo "cloning ${params.buildRepo} over http"
                                                          try { git([ branch: "${params.buildBranch}", url: "http://${params.buildRepo}" ]) }
                                                          catch(e2) {
                                                              echo "Failed cloning ${params.buildRepo}#${params.buildBranch} - ${e2}"
                                                              throw e2
                                                          }
                                                      } else { throw e }
                                                  }
                                                  try {
                                                      gitCommitMsg = sh(returnStdout: true, script: "git log -n 1").trim()
                                                  } catch(e) { echo "In non-critical catch block resolving commit message - ${e}" }
                                                  if (params.sonarHost != "") {
                                                      sh """
                                                      echo preparing to scan code ...
                                                      git checkout ${params.buildHash}
                                                      npm cache clean --force || echo WARNING: cache clean returned non-0
                                                      if grep ^install: Makefile >/dev/null; then
                                                          make install
                                                      else
                                                          npm install
                                                      fi
                                                      npm install xunit-file mocha expect || echo too bad
                                                      npm install sonarqube-scanner
                                                      test -d reports || mkdir -p reports
                                                      echo should run unit tests, but none available yet
                                                      """
                                                      try {
                                                          withCredentials([usernamePassword(credentialsId: "${params.sonarHost}-scanner", usernameVariable: 'SONAR_SCANNER_USERNAME', passwordVariable: 'SONAR_SCANNER_PASSWORD')]) {
                                                              sh """
                                                              echo "sonar.exclusions=src/node_modules/**, src/node_modules/**" >sonar-project.properties
                                                              echo sonar.host.url=${params.sonarProto}://${params.sonarHost}:${params.sonarPort} >>sonar-project.properties
                                                              echo "sonar.login=${env.SONAR_SCANNER_USERNAME}" >>sonar-project.properties
                                                              echo sonar.links.homepage=${cloneProto}://${params.buildRepo} >>sonar-project.properties
                                                              echo sonar.links.issue=${cloneProto}://${params.buildRepo}/issues >>sonar-project.properties
                                                              echo "sonar.password=${env.SONAR_SCANNER_PASSWORD}" >>sonar-project.properties
                                                              echo sonar.projectDescription=MaildropSMTP >>sonar-project.properties
                                                              echo sonar.projectKey=s2i-maildropsmtp >>sonar-project.properties
                                                              echo sonar.projectName=s2i-maildropsmtp >>sonar-project.properties
                                                              echo sonar.projectVersion=${params.buildBranch} >>sonar-project.properties
                                                              echo sonar.sources=./src >>sonar-project.properties
                                                              echo sonar.tests=tests >>sonar-project.properties
                                                              echo "sonar.test.exclusions=tests/**" >>sonar-project.properties
                                                              """
                                                          }
                                                      } catch (e) {
                                                          sh """
                                                          echo sonar credentials not found for ${params.sonarHost}, reporting anonymously
                                                          echo "sonar.exclusions=node_modules/**, src/node_modules/**" >sonar-project.properties
                                                          echo sonar.host.url=${params.sonarProto}://${params.sonarHost}:${params.sonarPort} >>sonar-project.properties
                                                          echo sonar.links.homepage=${cloneProto}://${params.buildRepo} >>sonar-project.properties
                                                          echo sonar.links.issue=${cloneProto}://${params.buildRepo}/issues >>sonar-project.properties
                                                          echo sonar.projectDescription=MaildropSMTP >>sonar-project.properties
                                                          echo sonar.projectKey=s2i-maildropsmtp >>sonar-project.properties
                                                          echo sonar.projectName=s2i-maildropsmtp >>sonar-project.properties
                                                          echo sonar.projectVersion=${params.buildBranch} >>sonar-project.properties
                                                          echo sonar.sources=./src >>sonar-project.properties
                                                          echo sonar.tests=tests >>sonar-project.properties
                                                          echo "sonar.test.exclusions=tests/**" >>sonar-project.properties
                                                          """
                                                      }
                                                      sh "JAVA_TOOL_OPTIONS='-XX:+UnlockExperimentalVMOptions -Dsun.zip.disableMemoryMapping=true' ./node_modules/.bin/sonar-scanner || true"
                                                  }
                                              }
                                              try { sh "test -d ${templatePath}" }
                                              catch (e) {
                                                  echo "Could not find ./openshift in ${params.buildRepo}#${params.buildBranch}"
                                                  throw e
                                              }
                                          }
                                      } catch(e) {
                                          echo "In catch fetching and scanning sources - ${e}"
                                          throw e
                                      }
                                  }
                              }
                          }
                      }
                  }
                  stage('create') {
                      steps {
                          script {
                              openshift.withCluster() {
                                  openshift.withProject() {
                                      def namespace = "${openshift.project()}"
                                      try {
                                          timeout(10) {
                                              def templatePath = "/home/jenkins/workspace/${namespace}/${namespace}-maildropsmtp-jenkins-pipeline/tmpmaildropsmtp${params.buildBranch}/openshift"
                                              def repoHost = params.buildRepo.split('/')[0]
                                              echo "Processing MaildropSMTP:${params.buildHash}, from ${repoHost}, tagging to ${params.buildBranch}"
                                              echo "Building from NodeJS:${params.nodeTag}"
                                              try {
                                                  echo " == Creating ImageStream =="
                                                  objectsFromTemplate = openshift.process("-f", "${templatePath}/imagestream.yaml")
                                                  echo "The template will create ${objectsFromTemplate.size()} objects"
                                                  created = openshift.apply(objectsFromTemplate)
                                                  created.withEach { echo "Created ${it.name()} with labels ${it.object().metadata.labels}" }
                                              } catch(e) { echo "In non-critical catch block while creating ImageStream - ${e}" }
                                              echo " == Creating BuildConfigs =="
                                              if (privateRepo) {
                                                  withCredentials([string(credentialsId: "git-${repoHost}", variable: 'GIT_TOKEN')]) {
                                                      objectsFromTemplate = openshift.process("-f", "${templatePath}/build-with-secret.yaml", '-p', "MAILDROPSMTP_REPOSITORY_REF=${params.buildHash}",
                                                          '-p', "MAILDROPSMTP_REPOSITORY_URL=${cloneProto}://${params.buildRepo}", '-p', "GIT_DEPLOYMENT_TOKEN=${GIT_TOKEN}", '-p', "NODE_IMAGESTREAM_TAG=nodejs:${params.nodeTag}")
                                                  }
                                              } else {
                                                  objectsFromTemplate = openshift.process("-f", "${templatePath}/build.yaml", '-p', "MAILDROPSMTP_REPOSITORY_REF=${params.buildHash}",
                                                      '-p', "MAILDROPSMTP_REPOSITORY_URL=${cloneProto}://${params.buildRepo}", '-p', "NODE_IMAGESTREAM_TAG=nodejs:${params.nodeTag}")
                                              }
                                              echo "The template will create ${objectsFromTemplate.size()} objects"
                                              for (o in objectsFromTemplate) { o.metadata.labels["${templateSel}"] = "${templateMark}-${params.buildHash}" }
                                              created = openshift.apply(objectsFromTemplate)
                                              created.withEach { echo "Created ${it.name()} from template with labels ${it.object().metadata.labels}" }
                                          }
                                      } catch(e) {
                                          echo "In catch block while creating resources - ${e}"
                                          throw e
                                      }
                                  }
                              }
                          }
                      }
                  }
                  stage('build') {
                      steps {
                          script {
                              openshift.withCluster() {
                                  openshift.withProject() {
                                      try {
                                          timeout(200) {
                                              echo "watching maildropsmtp-${params.buildHash} docker image build"
                                              def builds = openshift.selector("bc", [ name: "maildropsmtp-${params.buildHash}" ]).related('builds')
                                              builds.untilEach(1) { return (it.object().status.phase == "Complete") }
                                          }
                                      } catch(e) {
                                          echo "In catch block while building Docker image - ${e}"
                                          throw e
                                      }
                                  }
                              }
                          }
                      }
                  }
                  stage('tag') {
                      steps {
                          script {
                              if ("${params.buildBranch}" == "${params.buildHash}") { echo "skipping tag - source matches target" }
                              else {
                                  openshift.withCluster() {
                                      openshift.withProject() {
                                          try {
                                              timeout(5) {
                                                  def namespace = "${openshift.project()}"
                                                  retry(3) {
                                                      sh """
                                                      oc login https://kubernetes.default.svc.cluster.local --certificate-authority=/var/run/secrets/kubernetes.io/serviceaccount/ca.crt --token=\$(cat /var/run/secrets/kubernetes.io/serviceaccount/token) > /dev/null 2>&1
                                                      oc tag -n ${namespace} maildropsmtp:${params.buildHash} maildropsmtp:${params.buildBranch}
                                                      """
                                                  }
                                              }
                                          } catch(e) {
                                              echo "In catch block while tagging MaildropSMTP image - ${e}"
                                              throw e
                                          }
                                      }
                                  }
                              }
                          }
                      }
                  }
              }
              post {
                  always {
                      script {
                          openshift.withCluster() {
                              openshift.withProject() {
                                  def namespace   = "${openshift.project()}"
                                  def postJobName = "${namespace}/${namespace}-post-triggers-jenkins-pipeline"
                                  currentBuild.description = """
                                  ${params.buildRepo} ${params.buildBranch} (try ${params.jobRetryCount}/${params.jobMaxRetry})
                                  ${gitCommitMsg}
                                  """.stripIndent()
                                  echo "cleaning up assets for maildropsmtp-${params.buildHash}"
                                  sh "rm -fr /home/jenkins/workspace/${namespace}/${namespace}-maildropsmtp-jenkins-pipeline/tmpmaildropsmtp${params.buildBranch}"
                                  openshift.selector("buildconfigs", [ "${templateSel}": "${templateMark}-${params.buildHash}" ]).delete()
                                  openshift.selector("configmaps", [ "${templateSel}": "${templateMark}-${params.buildHash}" ]).delete()
                                  openshift.selector("deploymentconfigs", [ "${templateSel}": "${templateMark}-${params.buildHash}" ]).delete()
                                  openshift.selector("routes", [ "${templateSel}": "${templateMark}-${params.buildHash}" ]).delete()
                                  openshift.selector("secrets", [ "${templateSel}": "${templateMark}-${params.buildHash}" ]).delete()
                                  openshift.selector("services", [ "${templateSel}": "${templateMark}-${params.buildHash}" ]).delete()
                                  def jobParams = [
                                          [$class: 'StringParameterValue', name: "hasUpstream", value: "yes"],
                                          [$class: 'StringParameterValue', name: "jobMaxRetry", value: params.jobMaxRetry],
                                          [$class: 'StringParameterValue', name: "jobRetryCount", value: params.jobRetryCount],
                                          [$class: 'StringParameterValue', name: "jobStatus", value: currentBuild.currentResult],
                                          [$class: 'StringParameterValue', name: "sourceBranch", value: params.buildBranch],
                                          [$class: 'StringParameterValue', name: "sourceComponent", value: "maildropsmtp"],
                                          [$class: 'StringParameterValue', name: "sourceRef", value: params.buildHash],
                                          [$class: 'StringParameterValue', name: "sourceRepo", value: params.buildRepo],
                                          [$class: 'StringParameterValue', name: "upstreamSourceBranch", value: params.nodeTag],
                                          [$class: 'StringParameterValue', name: "upstreamSourceComponent", value: "node"]
                                      ]
                                  try { build job: postJobName, parameters: jobParams, propagate: false, wait: false }
                                  catch(e) { echo "caught ${e} starting Job post-process" }
                              }
                          }
                      }
                  }
                  changed { echo "changed?" }
                  failure { echo "Build failed (${params.jobRetryCount} out of ${params.jobMaxRetry})" }
                  success { echo "success!" }
                  unstable { echo "unstable?" }
              }
          }
      type: JenkinsPipeline
parameters:
- name: CICD_NAMESPACE
  description: CICD Namespace hosting Jenkins custom agent images
  displayName: CICD Namespace
  required: true
  value: ci
- name: GIT_REPOSITORY
  description: Git Repostory URL, Relative to GIT_SOURCE_HOST
  displayName: Git Repository
  value: synacksynack/opsperator/s2i-maildropsmtp.git
- name: GIT_SOURCE_HOST
  description: Git FQDN we would build images from
  displayName: Git Server
  value: gitlab.com
- name: JENKINS_SONAR_IMAGE
  value: jenkins-agent-node-sonar:master
- name: OPENSHIFT_REGISTRY
  description: OpenShift Registry
  displayName: Registry Address
  required: true
  value: "docker-registry.default.svc:5000"
- name: OPENSHIFT_ROUTED_DOMAIN
  description: OpenShift Routers-served Root Domain
  displayName: CI Router Domain
  value: ci.apps.intra.unetresgrossebite.com
- name: SONAR_ENDPOINT
  description: SonarQube Host Address
  displayName: SonarQube Host Endpoint
  value: sonarqube
- name: SONAR_PORT
  description: SonarQube Service Port
  displayName: SonarQube Port
  value: "9000"
- name: SONAR_PROTO
  description: SonarQube Service Proto
  displayName: SonarQube Proto
  value: http
