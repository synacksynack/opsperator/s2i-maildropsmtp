#!/bin/sh

ALTINBOX_MOD=${ALTINBOX_MOD:-1}
LOG=${LOG_LEVEL:-info}
MAILDROP_DOMAIN=${MAILDROP_DOMAIN:-maildrop.demo.local}
MAILBOX_SIZE=${MAILBOX_SIZE:-100}
MAILBOX_TTL=${MAILBOX_TTL:-86400}
REDIS_HOST=${REDIS_HOST:-127.0.0.1}
REDIS_PORT=${REDIS_PORT:-6379}
SMTP_PORT=${SMTP_PORT:-1025}
STATS_DOMAIN=${STATS_DOMAIN:-md-stats.demo.local}
WS_PORT=${WS_PORT:-8080}
WSS_PORT=${WSS_PORT:-443}

if test "$DEBUG"; then
    set -x
    sed -i 's|info|debug|' src/config/log.ini
fi

grep -R maildrop.cc src | awk -F: '{print $1}' | while read f
    do
	sed -i "s|maildrop.cc|$MAILDROP_DOMAIN|g" "$f"
    done

sed -i "/^altinbox=/s/=.*/=$ALTINBOX_MOD/" \
    src/config/altinbox.ini

sed -i "/^stats_redis_host=/s/=.*/=$REDIS_HOST:$REDIS_PORT/" \
    src/config/dnsbl.ini

sed -i \
    -e "/^host=/s/=.*/=$REDIS_HOST/" \
    -e "/^port=/s/=.*/=$REDIS_PORT/" \
    src/config/greylist.ini

if test "$WS_PORT"; then
    if ! grep "^listen=\[::\]:$WS_PORT" src/config/http.ini >/dev/null 2>&1; then
	echo "listen=[::]:$WS_PORT" >src/config/http.ini
    fi
    if ! grep ^watch src/config/plugins >/dev/null 2>&1; then
	echo '' >>src/config/plugins
	echo watch >>src/config/plugins
    fi
    cat <<EOF >src/config/watch.ini
[wss]
url=ws://$STATS_DOMAIN:$WS_PORT
url=wss://$STATS_DOMAIN:$WSS_PORT
EOF
fi

sed -i \
    -e "/^mailbox_size=/s/=.*/=$MAILBOX_SIZE/" \
    -e "/^mailbox_ttl=/s/=.*/=$MAILBOX_TTL/" \
    src/config/queue.redis.ini

sed -i \
    -e "/^host=/s/=.*/=$REDIS_HOST/" \
    -e "/^port=/s/=.*/=$REDIS_PORT/" \
    src/config/redis.ini

echo "listen=[::0]:$SMTP_PORT" \
    >>src/config/smtp.ini

./node_modules/.bin/haraka -c src
