IMAGE = opsperator/maildropsmtp

.PHONY: install
install:
	@@NODE_ENV=build npm install --unsafe-perm

.PHONY: prep-runtime
prep-runtime:
	echo OK

.PHONY: run
run: prep-runtime
	./startup.sh

.PHONY: start
start: run

.PHONY: build
	docker build -t $(IMAGE) -f Dockerfile.debug-s2i .

.PHONY: dockertest
dockertest: build
	docker run -p1025:1025 -e REDIS_HOST=10.42.42.127 \
	    -e REDIS_PORT=6379 --entrypoint /usr/libexec/s2i/run \
	    -it $(IMAGE):latest

.PHONY: dockerteststats
dockerteststats: build
	docker run -p1025:1025 -p8080:8080 -e REDIS_HOST=10.42.42.127 \
	    -e REDIS_PORT=6379 -e WS_PORT=8080 -e WS_PROTO=ws \
	    -e DEBUG=yay --entrypoint /usr/libexec/s2i/run \
	    -it $(IMAGE)

.PHONY: kubebuild
kubebuild: kubecheck
	@@for f in image git task pipeline pipelinerun; \
	    do \
		kubectl apply -f deploy/kubernetes/tekton-$$f.yaml; \
	    done

.PHONY: kubecheck
kubecheck:
	@@kubectl version >/dev/null 2>&1 || exit 42

.PHONY: kubedeploy
kubedeploy: kubecheck
	@@for f in service configmap statefulset deployment; \
	    do \
		kubectl apply -f deploy/kubernetes/$$f.yaml; \
	    done

.PHONY: test
test:
	@@echo no tests
