# k8s Maildrop SMTP

Maildrop SMTP server

Forked from gitlab.com/markbeeson/maildrop/

Environment variables and volumes
----------------------------------

The image recognizes the following environment variables that you can set during
initialization by passing `-e VAR=VALUE` to the Docker `run` command.

|    Variable name      |    Description                       | Default               |
| :-------------------- | ------------------------------------ | --------------------- |
|  `ALTINBOX_MOD`       | m242/Maildrop Alt Inbox ID           | `1`                   |
|  `MAILBOX_SIZE`       | m242/Maildrop max messages per inbox | `100`                 |
|  `MAILBOX_TTL`        | m242/Maildrop ttl for messages       | `86400`               |
|  `MAILDROP_DOMAIN`    | m242/Maildrop SMTP FQDN              | `maildrop.demo.local` |
|  `REDIS_HOST`         | m242/Maildrop redis backend          | `127.0.0.1`           |
|  `REDIS_PORT`         | m242/Maildrop redis port             | `6379`                |
|  `SMTP_PORT`          | m242/Maildrop SMTP port              | `1025`                |
|  `STATS_DOMAIN`       | m242/Maildrop stats FQDN             | `md-stats.demo.local` |
|  `WS_PORT`            | m242/Maildrop stats websocket port   | `8080`                |
|  `WSS_PORT`           | m242/Maildrop public stats port      | `443`                 |
